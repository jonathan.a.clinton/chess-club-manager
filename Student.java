/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ChessManager;

/**
 *
 * @author jonathan
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Student {

    public Student() {
        this.gameHistory = new GameList();
        name = "";
        grade = 0;
        rating = 1000;
    }
    public Student(String student){
        String s = student;
        name = s;
        //name = s.substring(0, s.indexOf("\n"));
        /*s = s.substring(s.indexOf("\n"));
        
        grade = Integer.parseInt(s.substring(0, s.indexOf("\n")));
        s = s.substring(s.indexOf("\n"));
        rating = Integer.parseInt(s.substring(0, s.indexOf("\n")));
        s = s.substring(s.indexOf("\n"));
        id = Integer.parseInt(s.substring(0, s.indexOf("\n")));*/
    }
    public Student(String n, int g, double r, int i){
        name = n;
        grade = g;
        rating = r;
        id = i;
    }
    
    /*public Object clone() throws CloneNotSupportedException{
        Object p = super.clone();
        Student casted = (Student)p;
        casted.name = this.name;
        casted.grade = this.grade;
        casted.rating = this.rating;
        casted.id = this.id;
        casted.gameHistory = (GameList)this.gameHistory.clone();
    }*/

	String printName(){
		return name;
	}
	String printGrade(){
		return Integer.toString(grade);
	}
	String printRating(){
		return Double.toString(rating);
	}
	String printHistory(){
		return gameHistory.toString();
	}
        String printID(){
            return Integer.toString(id);
        }
        
        public void save(int i){
            //saves "this" student
            //open a file
            //store student information as a string
            //close file
            //TODO
            id = i;
            String filename = name + Integer.toString(id) + ".txt";
            //BufferedWriter out = null;
            try  
            {
                File file = new File(filename);
                if(!file.exists()){
                    file.createNewFile();
                }
                FileWriter fstream = new FileWriter(file.getAbsoluteFile());
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(this.toString());
                /*FileWriter fstream = new FileWriter(filename); //true tells to append data.
                out = new BufferedWriter(fstream);
                out.write(this.toString());
                System.err.println("trying to write out " + this.toString() + "\nto file: " + filename);
                */
                out.close();
            }
            catch (IOException e)
            {
                System.err.println("Error: " + e.getMessage());
            }
            finally
            {
                /*if(out != null) {
                    out.close();
                }*/
            }
            
            //System.load(filename);
            //now how do I access data
            
        }
        public Student load(String filename){
            //loads a student from a given file and creates that object
            //open a file
            //load string of information
            //pass string to student constructor
            //TODO
            //String temp = new Student().toString(); //will hold student string
            return new Student();
        }
        public void readFromFile(){
           BufferedReader in = null;
            try  
            {
                BufferedReader istream = new BufferedReader(new FileReader(name)); 
                String line;
                if((line = istream.readLine()) != null){
                    name = line;
                }
                if((line = istream.readLine()) != null){
                    grade = Integer.parseInt(line);
                }
                if((line = istream.readLine()) != null){
                    rating = Double.parseDouble(line);
                }
                if((line = istream.readLine()) != null){
                    id = Integer.parseInt(line);
                }else{
                    System.err.println("incorrectly read from student file");
                }
                istream.close();
            }
            catch (IOException e)
            {
                System.err.println("Error: " + e.getMessage());
            }
            finally
            {
                /*if(out != null) {
                    out.close();
                }*/
            } 
        }
    @Override
	public String toString(){
                String temp = printName() +"\n";
                temp = temp + printGrade() +"\n";
                temp = temp + printRating() +"\n";
                //temp = temp + printHistory() + "\n;
                temp = temp + printID() + "\n";
                return temp;//TODO
	}
        public String display(){
            String temp = "name: " + printName() +", ";
                temp = temp + "grade: " + printGrade() +", ";
                temp = temp + "rating: " + printRating() +"\n\n";
                //temp = temp + printHistory() + "\n;
                //temp = "ID: " + temp + printID() + "\n";
                return temp;
        }

	void editName(String n){
		name = n;
	}
	void editGrade(int g){
		grade = g;
	}
	void editRating(double change){
		rating = rating + change;
	}
	void addGame(game g){
		gameHistory.addGame(g);
	}
	
	String name;
	int grade;
	double rating;
	GameList gameHistory;
        int id;
	
	
}

