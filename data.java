/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ChessManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author jonathan
 */
public class data {
    public data(){
        students = new StudentList();
        games = new GameList();
        //TODO grab data and fill in
    }
    public data(String d){
        //TODO
    }
    public StudentList getStudents(){
        return students;
    }
    public void save(){
            for(int i = 0; i< students.getSize(); i++){
                students.getStudent(i).save(i);
            }
            BufferedWriter out = null;
            try  
            {
                FileWriter fstream = new FileWriter("nameList.txt");
                out = new BufferedWriter(fstream);
                for(int i = 0; i< students.getSize(); i++){
                    out.write(students.getStudent(i).name + 
                            Integer.toString(i)+ ".txt"+ "\n");
                }
                out.close();
                
                
                
            }
            catch (IOException e)
            {
                System.err.println("Error: " + e.getMessage());
            }
            finally
            {
                try{
                    if(out != null) {
                        out.close();
                    }
                }
                catch(IOException e){
                    System.err.println("couldn't close BufferedWrither out");
                    //throw e;
                }
            }
            
    }
    public void load(){
            BufferedReader in = null;
            try  
            {
                in = new BufferedReader(new FileReader("nameList.txt")); 
                String line;
                while ((line = in.readLine()) != null)
                {
                    students.addStudent(new Student(line));
                }
                for(int i = 0; i< students.getSize(); ++i){
                    students.getStudent(i).readFromFile();
                }
                in.close();
            }
            catch (IOException e)
            {
                System.err.println("Error: " + e.getMessage());
            }
            finally
            {
                try{
                    if(in != null) {
                        in.close();
                    }
                }
                catch(IOException e){
                    System.err.println("couldn't close BufferedReader in");
                    //throw e;
                }
            }
    }
    public String[] getStudentNames(){
        ArrayList<String> temp = new ArrayList();
        for(int i = 0;i < students.getSize();i++){
            temp.add(students.getStudent(i).printName());
        }
        String[] names = new String[temp.size()];
        names = temp.toArray(names);
        return names;
    }
    public Student getStudent(String name){
        for(int i = 0; i < students.getSize(); i++){
            if(students.getStudent(i).name == name){
                return students.getStudent(i);
            }
        }
        Student temp = new Student();
        return temp;
    }
    public String printStudents(){
        return students.toString();
    }
    public String displayStudents(){
        return students.display();
    }
    public GameList getGames(){
        return games;
    }
    private StudentList students;
    private GameList games;
    
    
}
