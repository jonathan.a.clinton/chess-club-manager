/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ChessManager;

import java.util.ArrayList;
//import com.thoughtworks.xstream;
//thinking about using this for persistence 
//otherwise each part could have a toString and a fromString function
/**
 *
 * @author jonathan
 */
public class StudentList {
    public StudentList(){
        students = new ArrayList<>();
    }
    public StudentList(String list){
        //TODO
    }
    public void addStudent(Student s){
        students.add(s);
    }
    public void removeStudent(Student s){
        students.remove(s);
    }
    public Student getStudent(int index){
        return students.get(index);
    }
    public int getSize(){
        return students.size();
    }
    @Override
    public String toString(){
        String temp = new String("");
        for(int i = 0; i < students.size(); i++){
            temp = temp + students.get(i).toString();
        }
        return temp;
    }
    public String display(){
        String temp = new String("");
        for(int i = 0; i < students.size(); i++){
            temp = temp + students.get(i).display();
        }
        return temp;
    }
    private ArrayList<Student> students;
}
